#!/bin/bash

# https://githubmemory.com/repo/pypa/pip/issues/10219
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

release_id_like=$(awk -F= '/^ID_LIKE=/ {print $2}' /usr/lib/os-release)

if [ "${release_id_like}" = 'debian' ] ;then
    sudo apt install python3.8-venv
    /usr/bin/python3 -m venv --system-site-packages ~/.venv/bootstrap-ansible
else
    /usr/libexec/platform-python -m venv --system-site-packages ~/.venv/bootstrap-ansible
fi

source ~/.venv/bootstrap-ansible/bin/activate

pip install --upgrade pip setuptools

# Install v3.4.0 to avoid deprecation warnings with
# system-installed Python v3.6.
pip install 'ansible==3.4.0'

ansible -bm package -a "name=git" -i localhost, -c local localhost

mkdir ~/git

cd ~/git

git clone https://gitlab.com/mbm329/bootstrap-ansible.git

ansible-playbook ~/git/bootstrap-ansible/bootstrap-system.yml
