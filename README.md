# bootstrap-ansible
An egg to hatch the chicken.

# Usage
1. Run directly from remote repo.
```
curl -fsSL https://gitlab.com/mbm329/bootstrap-ansible/-/raw/main/bootstrap-ansible.sh | bash -s
```
You now have Ansible in a Python virtual environment and Git installed.

2. Activate the python virtual environment.
```
source ~/.venv/bootstrap-ansible/bin/activate
```

3. Clone your Ansible repo used for laying down your working-environment.
Example:
```
git clone https://gitlab.com/mbm329/ansible-homedir.git ~/git/ansible-homedir
```

4. Run Ansible to deploy your working-environment (including updated Python, Ansible, Ansible config, git config, etc...)
Example:
```
ansible-playbook ~/git/ansible-homedir/pb-ctrl.yml -i ~/git/ansible-homedir/inventory/lab/hosts --ask-vault-password
```

